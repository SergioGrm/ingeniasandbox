// primera.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "MovilRigido.h"
#include "MovilBlando.h"

int main(int argc, char* argv[])
{
	std::vector< std::shared_ptr<Movil> > vector;

	vector.push_back( std::shared_ptr<Movil>(new MovilBlando()) );
	vector.push_back( std::shared_ptr<Movil>(new MovilRigido()) );

	auto ite = vector.begin();
	auto end = vector.end();

	for ( ; ite != end; ++ite) 
	{
		ite->get()->simular(0.01);
		printf("X = %f, Y = %f\n", ite->get()->x, ite->get()->y);
	}
	
	system("pause");
	return 0;
}

int principal3(int argc, char* argv[])
{
	std::vector<Movil*> vector;

	vector.push_back( new MovilBlando() );
	vector.push_back( new MovilRigido() );

	auto ite = vector.begin();
	auto end = vector.end();

	for ( ; ite != end; ++ite) 
	{
		(*ite)->simular(0.01);
		printf("X = %f, Y = %f\n", (*ite)->x, (*ite)->y);
		delete *ite;
	}
	
	system("pause");
	return 0;
}

int principal2(int argc, char* argv[])
{
	Movil *pmo = new MovilBlando();

	pmo->simular(0.01);
	printf("X = %f, Y = %f\n", pmo->x, pmo->y);

	delete pmo;

	system("pause");
	return 0;
}

int principal1()
{
	const double inc_t = 0.01;

	std::vector<MovilRigido> v;

	MovilRigido mo;
	v.push_back(mo);
	mo.x = 4.0;
	v.push_back(mo);

	auto ite = v.begin();
	auto end = v.end();
	for ( ; ite != end; ++ite)
	{
		ite->simular(inc_t);
		printf("X = %f, Y = %f\n", ite->x, ite->y);
	}

	//printf("X = %f\n", v.at(3).x);
	//v.pop_back();
	//m1.simular(inc_t);
	system("pause");
	return 0;
}
